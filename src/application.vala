/* application.vala
 *
 * Copyright 2021 Philipp Unger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Snoop
{
  public class Application : Adw.Application
  {
    public static string parameter_path = "";

    private ActionEntry[] APP_ACTIONS =
    {
      { "about", on_about_action },
      { "preferences", on_preferences_action },
      { "quit", quit }
    };


    public Application (string parameter_path)
    {
      Object (application_id: "de.philippun1.Snoop", flags: ApplicationFlags.FLAGS_NONE);

      this.add_action_entries(this.APP_ACTIONS, this);
      this.set_accels_for_action("app.quit", {"<primary>q"});
      this.set_accels_for_action("app.preferences", {"<primary>comma"});

      Application.parameter_path = parameter_path;

      application_id = Config.APPLICATION_ID;
    }

    public override void activate ()
    {
      base.activate();
      var win = this.active_window;
      if (win == null)
      {
        win = new Snoop.Window (this);
      }
      win.present ();
    }

    private void on_about_action ()
    {
      string[] developers = {"Philipp Unger"};
      string[] designers = {"Philipp Unger"};
      string application_name = _("Snoop");

#if ADWAITA_ABOUT_WINDOW
      var about = new Adw.AboutWindow() {
        designers = designers,
        developers = developers,
        translator_credits = _("translator-credits"),
        copyright = "\xc2\xa9 2024 Philipp Unger",
        license_type = Gtk.License.GPL_3_0,
        application_name = application_name,
        application_icon = application_id,
        version = Config.VERSION,
        website = "https://gitlab.gnome.org/philippun1/snoop",
        issue_url = "https://gitlab.gnome.org/philippun1/snoop/-/issues"
      };
      about.show();
#else
      // fallback to Gtk.AboutDialog if adwaita version < 1.2
      Gtk.show_about_dialog(this.active_window,
                          "program-name", application_name,
                          "authors", developers,
                          "artists", designers,
                          "version", Config.VERSION);
#endif
    }

    private void on_preferences_action ()
    {
      var preferences_window = new Snoop.Preferences (this);
      preferences_window.show();
    }
  }
}

