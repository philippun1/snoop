""" snoop nautilus extension """
import os
import subprocess
from gi.repository import Nautilus, GObject, Gio

class SnoopMenuProvider(GObject.GObject, Nautilus.MenuProvider):
    """ snoop menu provider for Nautilus """

    exe = "flatpak run de.philippun1.Snoop"

    def __init__(self):
        try:
            output = subprocess.check_output(["flatpak", "list"]).decode()
            if "de.philippun1.Snoop" in output:
                return
        except Exception as _ex:
            pass

        # fallback to non flatpak snoop
        self.exe = "snoop"

    def __open_snoop(self, file):
        os.system(f"""{self.exe} "{file.get_location().get_path()}" &""")

    def menu_activate_cb(self, menu, file):
        self.__open_snoop(file)

    def menu_background_activate_cb(self, menu, file):
        self.__open_snoop(file)

    def __create_sub_menu(self, file, additional):
        if file.get_file_type()== Gio.FileType.DIRECTORY:
            item = Nautilus.MenuItem(name="SnoopMenuProvider::Search::" + additional,
                                         label="Snoop...",
                                         tip="",
                                         icon="search-symbolic")
            item.connect("activate", self.menu_activate_cb, file)

            return item

    def get_file_items(self, files):
        if len(files) == 1:
            item = self.__create_sub_menu(files[0], "file")

            return item,

    def get_background_items(self, file):
        item = self.__create_sub_menu(file, "background")

        return item,
